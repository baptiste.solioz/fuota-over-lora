#ifndef __COMMON_H
#define __COMMON_H

#define METHOD_W_ADDR

/** Start address of application space in flash */
#define APP_ADDRESS (uint32_t) 0x8004000

#define FUOTA_ADDRESS (uint32_t) 0x800A000

#define DATA_ADDRESS (uint32_t) 0x8003000

#define MAXPAGE 32 * 1024 / 128 			//Nb page available for 32K

/*!
 * FUOTA DATA STRUCTURE
 */
typedef struct Fuota_data_s
{
    bool		IsFuotaPerformed;
    uint8_t		DataSize;
    uint16_t	DataNb;
    uint32_t 	DataPtr[MAXPAGE];
}Fuota_data_t;


#endif //__COMMON_H
