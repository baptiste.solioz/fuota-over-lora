/**
 *******************************************************************************
 * STM32 Bootloader Source
 *******************************************************************************
 * @author Baptiste Solioz
 * @file   virtualCom.c
 * @brief  This file contains a refined implementation of the virtual com port
 *
 *******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "virtualCom.h"

/* Private typedef -----------------------------------------------------------*/
static UART_HandleTypeDef UartHandle;

/* Functions -----------------------------------------------------------------*/

/* -----------------------------------------------------------------------------
 * @name   vcom_init
 * @brief  Initialization of the virtual  COM Port
 * @return None
------------------------------------------------------------------------------*/
void vcom_Init()
{
	GPIO_InitTypeDef  GPIO_InitStruct = {0};
  /* Enable GPIO TX/RX clock */
  __GPIOA_CLK_ENABLE();
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = USARTx_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = USARTx_TX_AF;

  HAL_GPIO_Init(USARTx_TX_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = USARTx_RX_PIN;
  GPIO_InitStruct.Alternate = USARTx_RX_AF;

  HAL_GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStruct);

  /*## Configure the UART peripheral ######################################*/
  /* Put the USART peripheral in the Asynchronous mode (UART Mode) */
  /* UART1 configured as follow:
      - Word Length = 8 Bits
      - Stop Bit = One Stop bit
      - Parity = ODD parity
      - BaudRate = 921600 baud
      - Hardware flow control disabled (RTS and CTS signals) */
  UartHandle.Instance        = USART2;

  UartHandle.Init.BaudRate   = 115200;
  UartHandle.Init.WordLength = UART_WORDLENGTH_8B;
  UartHandle.Init.StopBits   = UART_STOPBITS_1;
  UartHandle.Init.Parity     = UART_PARITY_NONE;
  UartHandle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  UartHandle.Init.Mode       = UART_MODE_TX;

  if (HAL_UART_Init(&UartHandle) != HAL_OK)
  {
    /* Initialization Error */
  }
}

/* -----------------------------------------------------------------------------
 * @name   vcom_DeInit
 * @brief  Uninitalize the Virtual COM Port
 * @return None
------------------------------------------------------------------------------*/
void vcom_DeInit(void)
{
  HAL_UART_DeInit(&UartHandle);
}

/* -----------------------------------------------------------------------------
 * @name   	vcom_Transmit
 * @brief  	Transmit the string given in parameter through the UART Port
 * @param	String to send
 * @return 	None
------------------------------------------------------------------------------*/
void vcom_Transmit(const char* str)
{
    HAL_UART_Transmit(&UartHandle, (uint8_t*)str, (uint16_t)strlen(str), 100);
}
