/*
 * interruptHandler.h
 *
 *  Created on: 12 mai 2020
 *      Author: bapti
 */

#ifndef APPLICATION_BOOTLOADER_INTERRUPTHANDLER_H_
#define APPLICATION_BOOTLOADER_INTERRUPTHANDLER_H_

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  HAL_IncTick();
}

void HardFault_Handler(void)
{
  while (1)
  {
    __NOP();
  }

}

#endif /* APPLICATION_BOOTLOADER_INTERRUPTHANDLER_H_ */
