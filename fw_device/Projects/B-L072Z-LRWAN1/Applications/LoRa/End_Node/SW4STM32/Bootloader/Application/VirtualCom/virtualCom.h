/*
 * virtualCom.h
 *
 *  Created on: 12 mai 2020
 *      Author: bapti
 */

#ifndef VIRTUALCOM_H_
#define VIRTUALCOM_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "stm32l0xx.h"

#define USARTx_TX_PIN                  GPIO_PIN_2
#define USARTx_TX_GPIO_PORT            GPIOA
#define USARTx_TX_AF                   GPIO_AF4_USART2
#define USARTx_RX_PIN                  GPIO_PIN_3
#define USARTx_RX_GPIO_PORT            GPIOA
#define USARTx_RX_AF                   GPIO_AF4_USART2

void vcom_Init();
void vcom_DeInit(void);
void vcom_Transmit(const char* str);

#endif /* VIRTUALCOM_H_ */
