/**
 *******************************************************************************
 * STM32 Bootloader Source
 *******************************************************************************
 * @author Baptiste Solioz
 * @file   bootloader.c
 * @brief  This file contains a user implementation of the bootloader.
 *	       The bootloader implementation uses the official HAL library of ST.
 *
 *******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "bootloader.h"

/* Private defines -----------------------------------------------------------*/
#define REFLASH_FUOTA

#define USE_VCP 1

#define TEST_BOOTLOADER 0

/* Private typedef -----------------------------------------------------------*/
typedef void (*pFunction)(void); /*!< Function pointer definition */

#define     __IO    volatile

/* Private variables ---------------------------------------------------------*/
#ifdef REFLASH_FUOTA
uint8_t rawData[128];
uint8_t dataToWrite[128];
uint8_t addrToWrite[4];
#endif

Fuota_data_t* dataBoot;

/**
 * @brief  This function initializes bootloader and flash.
 * @return Bootloader error code ::eBootloaderErrorCodes
 * @retval BL_OK is returned in every case
 */
/* Functions -----------------------------------------------------------------*/

/* -----------------------------------------------------------------------------
 * @name   	Bootloader_Init
 * @brief  	This function initializes bootloader, the clock and the BSP.
 * @param	None
 * @return 	BL_OK if initialization done correctly
------------------------------------------------------------------------------*/
uint8_t Bootloader_Init(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};

	/* Enable HSE Oscillator and Activate PLL with HSE as source */
	RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSEState            = RCC_HSE_OFF;
	RCC_OscInitStruct.HSIState            = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL          = RCC_PLLMUL_6;
	RCC_OscInitStruct.PLL.PLLDIV          = RCC_PLLDIV_3;

	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
	  Bootloader_Error_Handler();
	}

	/* Set Voltage scale1 as MCU will run at 32MHz */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/* Poll VOSF bit of in PWR_CSR. Wait until it is reset to 0 */
	while (__HAL_PWR_GET_FLAG(PWR_FLAG_VOS) != RESET) {};

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
	clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	{
	  Bootloader_Error_Handler();
	}

    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_USART2_CLK_ENABLE();

    BSP_LED_Init(LED1);
    BSP_LED_Init(LED2);
    BSP_LED_Init(LED3);
    BSP_LED_Init(LED4);

    /* Check system reset flags */
    if(__HAL_RCC_GET_FLAG(RCC_FLAG_OBLRST))
    {
        Bootloader_print("OBL flag is active.\n");
#if(CLEAR_RESET_FLAGS)
        /* Clear system reset flags */
        __HAL_RCC_CLEAR_RESET_FLAGS();
        Bootloader_print("Reset flags cleared.\n");
#endif
    }

    return BL_OK;
}

/* -----------------------------------------------------------------------------
 * @name   	Bootloader_CheckSize
 * @brief  	This function initializes bootloader, the clock and the BSP.
 * @param	Size of the application
 * @return 	BL_OK if initialization done correctly, BL_SIZE_ERROR if not
------------------------------------------------------------------------------*/
uint8_t Bootloader_CheckSize(uint32_t appsize)
{
    return ((FLASH_BASE + FLASH_SIZE - APP_ADDRESS) >= appsize) ? BL_OK
                                                                : BL_SIZE_ERROR;
}

/**
 * @brief  This function checks whether a valid application exists in flash.
 *         The check is performed by checking the very first DWORD (4 bytes) of
 *         the application firmware. In case of a valid application, this DWORD
 *         must represent the initialization location of stack pointer - which
 *         must be within the boundaries of RAM.
 * @return Bootloader error code ::eBootloaderErrorCodes
 * @retval BL_OK: if first DWORD represents a valid stack pointer location
 * @retval BL_NO_APP: first DWORD value is out of RAM boundaries
 */
uint8_t Bootloader_CheckForApplication(void)
{
    return ((*(uint32_t*)APP_ADDRESS) != 0x00000000) ? BL_OK
                                                               : BL_NO_APP;
}

/**
 * @brief  This function checks whether a valid application exists in flash.
 *         The check is performed by checking the very first DWORD (4 bytes) of
 *         the application firmware. In case of a valid application, this DWORD
 *         must represent the initialization location of stack pointer - which
 *         must be within the boundaries of RAM.
 * @return Bootloader error code ::eBootloaderErrorCodes
 * @retval BL_OK: if first DWORD represents a valid stack pointer location
 * @retval BL_NO_APP: first DWORD value is out of RAM boundaries
 */
uint8_t Bootloader_CheckForFuota(void)
{
    return ((*(uint32_t*)FUOTA_ADDRESS) != 0x00000000) ? BL_OK
                                                               : BL_NO_APP;
}

/**
 * @brief  This function performs the jump to the user application in flash.
 * @details The function carries out the following operations:
 *  - De-initialize the clock and peripheral configuration
 *  - Stop the systick
 *  - Set the vector table location (if ::SET_VECTOR_TABLE is enabled)
 *  - Sets the stack pointer location
 *  - Perform the jump
 */
void Bootloader_JumpToApplication(void)
{
    uint32_t  JumpAddress = *(__IO uint32_t*)(APP_ADDRESS + 4);
    pFunction Jump        = (pFunction)JumpAddress;

    HAL_RCC_DeInit();
    HAL_DeInit();

    SysTick->CTRL = 0;
    SysTick->LOAD = 0;
    SysTick->VAL  = 0;

#if(SET_VECTOR_TABLE)
    SCB->VTOR = APP_ADDRESS;
#endif

    __set_MSP(*(__IO uint32_t*)APP_ADDRESS);
    __disable_irq();
    Jump();
}

/**
 * @brief  This function performs the jump to the user application in flash.
 * @details The function carries out the following operations:
 *  - De-initialize the clock and peripheral configuration
 *  - Stop the systick
 *  - Set the vector table location (if ::SET_VECTOR_TABLE is enabled)
 *  - Sets the stack pointer location
 *  - Perform the jump
 */
void Bootloader_JumpToFuota(void)
{
    uint32_t  JumpAddress = *(__IO uint32_t*)(FUOTA_ADDRESS + 4);
    pFunction Jump        = (pFunction)JumpAddress;

    HAL_RCC_DeInit();
    HAL_DeInit();

    SysTick->CTRL = 0;
    SysTick->LOAD = 0;
    SysTick->VAL  = 0;

#if(SET_VECTOR_TABLE)
    SCB->VTOR = FUOTA_ADDRESS;
#endif

    __set_MSP(*(__IO uint32_t*)FUOTA_ADDRESS);
    __disable_irq();
    Jump();
}

void BootloaderFlashNewApp()
{
	uint8_t retVal = 0;

	uint8_t * addrWrite = (uint8_t *)APP_ADDRESS;
	uint8_t * addrRead;

	uint32_t dataAddr = 0;
	uint32_t dataAddr_refact = 0;

#if(USE_VCP)
	char buf[100];
#endif /* USE_VCP */

	Bootloader_print("BootloaderFlashNewApp: R/W App from FUOTA\n");

	for(uint16_t i=0; i<dataBoot->DataNb; i++)
	{
		addrRead = (uint8_t *)dataBoot->DataPtr[i];
		retVal = FlashMemHandlerFct.Read(rawData, addrRead, 128);
		if(retVal != HAL_OK)
			Bootloader_print("BootloaderFlashNewApp : Read error\n");
		dataAddr = i*dataBoot->DataSize;
		for (uint16_t j = 0; j < dataBoot->DataSize; j++)
		{
#ifdef METHOD_W_ADDR
			dataAddr_refact = (dataAddr+j)%132;
			if(dataAddr_refact < 4)
				addrToWrite[dataAddr_refact] = rawData[j];
			else
				dataToWrite[dataAddr_refact-4] = rawData[j];
#else
			dataAddr_refact = (dataAddr+j)%128;
			dataToWrite[dataAddr_refact] = rawData[j];
#endif
			sprintf(buf, "%.2x, ", dataToWrite[dataAddr_refact]);
			Bootloader_print(buf);
			if(dataAddr_refact % 32 == 0 && dataAddr_refact != 0)
				Bootloader_print("\n");
#ifdef METHOD_W_ADDR
			if(dataAddr_refact == 127+4 && dataAddr != 0)
#else
			if(dataAddr_refact == 127 && dataAddr != 0)
#endif
			{
				HAL_Delay(100U);
#ifdef METHOD_W_ADDR
				addrWrite = (addrToWrite[0]<<24) + (addrToWrite[1]<<16) + (addrToWrite[2]<<8) + addrToWrite[3];
#endif
#if(USE_VCP)
				Bootloader_print("\n");
				sprintf(buf, "AddrWrite = %x \n", addrWrite);
				Bootloader_print(buf);
				Bootloader_print("\n");
#endif /* USE_VCP */
				retVal = FlashMemHandlerFct.Erase_Size((void *)addrWrite, 128);
				if(retVal == HAL_OK)
				{
					retVal = FlashMemHandlerFct.Write((void *)addrWrite, dataToWrite, 128);
					switch(retVal)
					{
						case HAL_OK: 			Bootloader_print("BootloaderFlashNewApp : Memory Erase successfully\r\n"); break;
						case HAL_ERROR : 		Bootloader_print("BootloaderFlashNewApp : ERROR memory not Erased\r\n"); break;
						case HAL_BUSY : 		Bootloader_print("BootloaderFlashNewApp : BUSY memory not Erased\r\n"); break;
						case HAL_TIMEOUT :		Bootloader_print("BootloaderFlashNewApp : TIMEOUT memory not Erased\r\n"); break;
						default : break;
					}
				}
				else
				{
					Bootloader_Error_Handler();
				}
				addrWrite += 128;
			}
		}
	}
#ifndef METHOD_W_ADDR
	retVal = FlashMemHandlerFct.Erase_Size((void *)addrWrite, 128);
	if(retVal == HAL_OK)
	{
		retVal = FlashMemHandlerFct.Write((void *)addrWrite, dataToWrite, 128);
		switch(retVal)
		{
			case HAL_OK: 			Bootloader_print("BootloaderFlashNewApp : Memory Erase successfully\r\n"); break;
			case HAL_ERROR : 		Bootloader_print("BootloaderFlashNewApp : ERROR memory not Erased\r\n"); break;
			case HAL_BUSY : 		Bootloader_print("BootloaderFlashNewApp : BUSY memory not Erased\r\n"); break;
			case HAL_TIMEOUT :		Bootloader_print("BootloaderFlashNewApp : TIMEOUT memory not Erased\r\n"); break;
			default : break;
		}
	}
	else
	{
		Bootloader_Error_Handler();
	}
#endif
	addrWrite = (uint8_t *)DATA_ADDRESS;
	retVal = FlashMemHandlerFct.Erase_Size((void *)addrWrite, sizeof(Fuota_data_t));
}

/* Main ----------------------------------------------------------------------*/
int main(void)
{
    HAL_Init();

    Bootloader_Init();

	BSP_LED_Off(LED1);
	BSP_LED_Off(LED2);
	BSP_LED_On(LED3);
	BSP_LED_On(LED4);
	HAL_Delay(1000U);

#if(USE_VCP)
    vcom_Init();
#endif /* USE_VCP */

    Bootloader_print("\nPower up, Boot started.\n");

#if (TEST_BOOTLOADER == 1)
    dataBoot = (Fuota_data_t *) &testData;
#else
    // Retrieve Fuota Data
    uint8_t* ptr = (uint8_t *)DATA_ADDRESS;
    dataBoot = (Fuota_data_t *) ptr;
#endif

    // Check if a fuota was performed
    if(dataBoot->IsFuotaPerformed)
    {
		/* Check if there is application in user flash area */
		if(Bootloader_CheckForFuota() == BL_OK)
		{
			Bootloader_print("Launching FUOTA.\n");

#ifdef REFLASH_FUOTA
			BootloaderFlashNewApp();
			HAL_Delay(1000U);
#endif
#if(USE_VCP)
			vcom_DeInit();
#endif /* USE_VCP */
			Bootloader_print("Start App from Fuota\n");
#ifdef REFLASH_FUOTA
			/* Launch application */
			Bootloader_JumpToApplication();
#else
			Bootloader_JumpToFuota();
#endif
		}
    }

    /* Check if there is application in user flash area */
    if(Bootloader_CheckForApplication() == BL_OK)
    {
        Bootloader_print("Launching Application.\n");

#if(USE_VCP)
        vcom_DeInit();
#endif /* USE_VCP */

        /* Launch application */
        Bootloader_JumpToApplication();
    }

    /* No application found */
    Bootloader_print("No application in flash.\n");

    Bootloader_Error_Handler();
}

void Bootloader_Error_Handler(void)
{
	Bootloader_print("Error_Handler\n\r");
	while (1)
	{
    	BSP_LED_On(LED1);
    	BSP_LED_On(LED2);
    	BSP_LED_Off(LED3);
    	BSP_LED_Off(LED4);
    	HAL_Delay(1000U);
    	BSP_LED_Off(LED1);
    	BSP_LED_Off(LED2);
    	BSP_LED_On(LED3);
    	BSP_LED_On(LED4);
    	HAL_Delay(1000U);
	}
}

/**
 * @brief  Debug over UART2 -> ST-LINK -> USB Virtual Com Port
 * @param  str: string to be written to UART2
 * @retval None
 */
void Bootloader_print(const char* str)
{
#if(USE_VCP)
	vcom_Transmit(str);
#endif /* USE_VCP */
}
