/**
 *******************************************************************************
 * STM32 Bootloader Header
 *******************************************************************************
 * @author Akos Pasztor
 * @file   bootloader.h
 * @brief  This file contains the bootloader configuration parameters,
 *	       function prototypes and other required macros and definitions.
 *
 * @see    Please refer to README for detailed information.
 *******************************************************************************
 * @copyright (c) 2020 Akos Pasztor.                    https://akospasztor.com
 *******************************************************************************
 */

#ifndef __BOOTLOADER_H
#define __BOOTLOADER_H

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "FlashMemHandler.h"
#include "b-l072z-lrwan1.h"
#include "virtualCom.h"
#include "interruptHandler.h"
#include "common.h"

/** Bootloader Configuration
 * @defgroup Bootloader_Configuration Bootloader Configuration
 * @{
 */

/** Automatically set vector table location before launching application */
#define SET_VECTOR_TABLE 1

/** Clear reset flags
 *  - If enabled: bootloader clears reset flags. (This occurs only when OBL RST
 * flag is active.)
 *  - If disabled: bootloader does not clear reset flags, not even when OBL RST
 * is active.
 */
#define CLEAR_RESET_FLAGS 1


#include "test_Flash.h"


/** @} */
/* End of configuration ------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* Include the appropriate header file */

#include "stm32l0xx.h"

/* Defines -------------------------------------------------------------------*/
/** Size of application in DWORD (32bits or 4bytes) */
#define APP_SIZE (uint32_t)(((END_ADDRESS - APP_ADDRESS) + 3) / 4)

/** Number of pages per bank in flash */
#define FLASH_PAGE_NBPERBANK (256)

/* MCU RAM information (to check whether flash contains valid application) */
#define RAM_BASE  (uint32_t)0x20000000     	/*!< Start address of RAM */
#define RAM_SIZE  20 * 1024 				/*!< RAM size in bytes */

/* Enumerations --------------------------------------------------------------*/
/** Bootloader error codes */
enum eBootloaderErrorCodes
{
    BL_OK = 0,      /*!< No error */
    BL_NO_APP,      /*!< No application found in flash */
    BL_SIZE_ERROR,  /*!< New application is too large for flash */
    BL_CHKS_ERROR,  /*!< Application checksum error */
    BL_ERASE_ERROR, /*!< Flash erase error */
    BL_WRITE_ERROR, /*!< Flash write error */
    BL_OBP_ERROR    /*!< Flash option bytes programming error */
};

/** Flash Protection Types */
enum eFlashProtectionTypes
{
    BL_PROTECTION_NONE  = 0,   /*!< No flash protection */
    BL_PROTECTION_WRP   = 0x1, /*!< Flash write protection */
    BL_PROTECTION_RDP   = 0x2, /*!< Flash read protection */
    BL_PROTECTION_PCROP = 0x4, /*!< Flash propietary code readout protection */
};

/* Functions -----------------------------------------------------------------*/
uint8_t Bootloader_Init(void);

uint8_t Bootloader_CheckSize(uint32_t appsize);
uint8_t Bootloader_CheckForFuota(void);
uint8_t Bootloader_CheckForApplication(void);
void 	Bootloader_JumpToFuota(void);
void    Bootloader_JumpToApplication(void);

void Bootloader_Error_Handler(void);
void Bootloader_print(const char* str);

#endif /* __BOOTLOADER_H */
