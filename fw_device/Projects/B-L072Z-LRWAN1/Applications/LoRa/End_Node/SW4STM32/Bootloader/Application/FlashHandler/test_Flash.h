/*
 * test_Flash.h
 *
 *  Created on: 7 mai 2020
 *      Author: bapti
 */

#ifndef APPLICATION_TEST_FLASH_H_
#define APPLICATION_TEST_FLASH_H_


static const Fuota_data_t testData = {
		.IsFuotaPerformed = true,
	    .DataSize = 112,
	    .DataNb = 5,
	    .DataPtr[0] = 0x800A000,
		.DataPtr[1] = 0x800A080,
		.DataPtr[2] = 0x800A100,
		.DataPtr[3] = 0x800A180,
		.DataPtr[4] = 0x800A200
};


#endif /* APPLICATION_TEST_FLASH_H_ */
