# FUOTA over LoRa

Firmware update over LoRa network using the B-072Z-LRWAN1 development board from STM.

The firmware project is based on the examples given by STM [](https://www.st.com/en/evaluation-tools/b-l072z-lrwan1.html) .

The network is based on the open-source application server and network server provided by ChirpStack [](https://www.chirpstack.io/). 

## Project Tree

There are two folders :

- `fw_device`: Main firmware project which contains a end-device example and a bootloader example which allows to handle the firmware update.
- `py_diffhex`: Python script which allows to compare two hex files and generate a binary file containing only the difference between the two files. A small header is added which corresponds to the position of the page in memory.

### fw_device

- `Drivers` : Drivers provided by STM. Hardware dependent.
- `Middlewares` : Contains the code provided by SEMTECH and related to the LoRaWAN protocol implementation.
- `Projects` : Code related to our projects. Separated in two distinct projects:
  - Bootloader used to handle the firmware update: `Projects\B-L072Z-LRWAN1\Applications\LoRa\End_Node\SW4STM32\Bootloader`
  - Main code used in particular to receive and handle the new firmware received: `Projects\B-L072Z-LRWAN1\Applications\LoRa\End_Node\SW4STM32\mlm32l07x01`