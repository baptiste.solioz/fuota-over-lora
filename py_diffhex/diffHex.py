VERSION = '1.0.0'

USAGE = '''diffHex: Create a bin file for LoRa Update by comparing two hex files and adding the block address before each block
Usage:
    python diffHex.py [options] HEXFILE HEXFILE
Options:
    -b, --block             block size (default 128 bytes)
    -o, --output            output file name (default new.bin)
    -s, --start             start of firmware address   (default 0x08000000)
    -e, --end               end of firmware address     (default 0x0802FFFF)
    -f, --fuota             size of Fuota space         (default 32768)
    -r, --rate              data rate parameter LoRa    (default DR_3)
    -h, --help              this help message.
    -v, --version           version info.
'''

import sys
import getopt
from intelhex import IntelHex, diff_dumps

def main(argv=None):

    blockSize = 128
    fileFuota = "new.bin"
    startAddr = 0x08000000
    endAddr = 0x0802FFFF
    fuotaSize = 32768
    dataRate = 112

    if argv is None:
        argv = sys.argv[1:]
    try:
        opts, args = getopt.gnu_getopt(argv, 'bosefrhv', ['block', 'ouptut', 'start', 'end', 'fuota', 'rate', 'help', 'version'])
        argnb = 0
        for o,a in opts:
            arg = args[argnb]
            #print("option : " + o + " is " + arg)
            argnb += 1
            if o in ('-h', '--help'):
                print(USAGE)
                return 0
            elif o in ('-v', '--version'):
                print(VERSION)
                return 0
            elif o in ('-b', '--block'):
                try:
                    if int(arg) % 128 == 0:
                        blockSize = int(arg)
                    else:
                        sys.stderr.write("Block must be a multiple of 128\n")
                        return 1
                except:
                    sys.stderr.write("Block is not numeric\n")
                    return 1
            elif o in ('-o', '--output'):
                fileFuota = arg
            elif o in ('-s', '--start'):
                try:
                    startAddr = int(arg, base=16)
                except:
                    sys.stderr.write("Start Address is not numeric\n")
                    return 1
            elif o in ('-e', '--end'):
                try:
                    endAddr = int(arg, base=16)
                except:
                    sys.stderr.write("End Address is not numeric\n")
                    return 1
            elif o in ('-f', '--fuota'):
                try:
                    fuotaSize = int(arg)
                except:
                    sys.stderr.write("Fuota Size is not numeric\n")
                    return 1
            elif o in ('-r', '--rate'):
                if arg in ('DR_0', 'DR_1', 'DR_2'):
                    dataRate = 51 - 3   # Max payload size for EU433 - 3 bytes (ChirpStack)
                elif arg == 'DR_3':
                    dataRate = 115 - 3  # Max payload size for EU433 - 3 bytes (ChirpStack)
                elif arg in ('DR_4', 'DR_5', 'DR_6', 'DR_7'):
                    dataRate = 222 - 3  # Max payload size for EU433 - 3 bytes (ChirpStack)
                else:
                    sys.stderr.write("DataRate incorrect\n")
                    return 1

    except getopt.GetoptError:
        e = sys.exc_info()[1]     # current exception
        sys.stderr.write(str(e)+"\n")
        sys.stderr.write(USAGE+"\n")
        return 1

    if len(args) != argnb+2:
        sys.stderr.write("ERROR: You should specify two files to summarize.\n")
        sys.stderr.write(USAGE+"\n")
        return 1

    fname1 = args[argnb]
    fname2 = args[argnb+1]
    
    ih1 = IntelHex(fname1)
    ih2 = IntelHex(fname2)
    
    try:
        fnew=open(fileFuota,"wb")
    except getopt.GetoptError:
        sys.stderr.write("Error when open new file for FUOTA : " + str(e) + "\n")

    totalBlock = 0
    for addr in range(startAddr, endAddr, blockSize):
        block_a = (ih1.tobinstr(start=addr, size=blockSize))
        block_b = (ih2.tobinstr(start=addr, size=blockSize))
        if block_a != block_b:
            for val in block_b:
                if val != 0xFF:
                    #print("Diff in block at addr = " + hex(addr))
                    totalBlock += 1
                    addrBytes = addr.to_bytes(4,'big')
                    fnew.write(addrBytes)
                    fnew.write(block_b)
                    break

    totalSize = totalBlock*(blockSize+4)
    availSize = (fuotaSize/blockSize*dataRate)
    if totalSize > availSize:
        sys.stderr.write("ERROR: Memory footprint too large " + totalSize + " instead of " + availSize + "\n")
        fnew.close()
        return 1
    print("Final Report :")
    print("Nb total Block different = "+ str(totalBlock))
    print("Size of the fuota file   = "+ str(totalSize))
    print("Total size available     = "+ str(availSize))
    print("File " + fileFuota + " ready for update")
    fnew.close()

if __name__ == '__main__':
    sys.exit(main())